



####################################################################################################
## Function to plot the results when simulating clincs' data by white noise.
####################################################################################################
plot.simResults <- function( simMatrix, realData = NULL,
                             xSim = NULL, xReal = NULL, xSimLab = NULL,
                             xlim = NULL, ylim = NULL,
                             outerQuantile = 1.0,
                             innerQuantile = 0.95,
                             title = "%nSims% Simulations",
                             xlab = "", ylab = "",
                             noXAxis = FALSE,
                             simColor = "grey", realColor = "black",
                             plotMedian = F, medianColor = "black",
                             plotType = "l",
                             quantileBoxWidth = 0.5, realSize = 1.5,
                             labStyle = 1 ) {

    simMatrix.nRows <- nrow(simMatrix)
    if ( is.null(rownames(simMatrix)) ) {
        rownames(simMatrix) <- c(1 : simMatrix.nRows)
    }
    if ( is.null(xSim) ) {
        xSim <- c(1 : simMatrix.nRows)
    }
    if ( is.null(xSimLab) ) {
        xSimLab <- rownames(simMatrix)
    }

    if ( !is.null(realData) ) {
        if ( is.null(names(realData)) ) {
            names(realData) <- c(1 : length(realData))
        }
        if ( is.null(xReal) ) {
            xReal <- c(1 : length(realData))
        }
    }

    nSims <- ncol(simMatrix)

    if (is.null(title)) {
        title <- ""
    } else {
        title <- gsub("%nSims%", nSims, title)
    }

    outerQuantile.hig <- NULL
    outerQuantile.low <- NULL
    if ( !is.null(outerQuantile) && !is.na(outerQuantile) ) {
        quantileOffset <- (1 - outerQuantile) / 2
        outerQuantile.low <-
            apply(simMatrix, 1, quantile, prob = quantileOffset)
        outerQuantile.hig <-
            apply(simMatrix, 1, quantile, prob = quantileOffset + outerQuantile)
    }

    innerQuantile.hig <- NULL
    innerQuantile.low <- NULL
    if ( !is.null(innerQuantile) && !is.na(innerQuantile) ) {
        quantileOffset <- (1 - innerQuantile) / 2
        innerQuantile.low <-
            apply(simMatrix, 1, quantile, prob = quantileOffset)
        innerQuantile.hig <-
            apply(simMatrix, 1, quantile, prob = quantileOffset + innerQuantile)
    }

    if (is.null(xlim)) {
        minX <- min( xSim )
        maxX <- max( xSim )
        xlim <- c(minX, maxX)
    }

    if (is.null(ylim)) {
        minY <- min( outerQuantile.low, innerQuantile.low )
        maxY <- max( outerQuantile.hig, innerQuantile.hig )
        ylim <- c(minY, maxY)
    }


    plot( NULL, main = title, las = labStyle,
          xlim = xlim, xlab = xlab, xaxt = "n",
          ylim = ylim, ylab = ylab )

    if (!noXAxis) {
        if (plotType == "b") {
            axis( 1, at = xSim, labels = xSimLab, las = labStyle )
        } else {
            xSimPretty = pretty(xSim)
            prettyIndex <- which(xSim %in% xSimPretty)
            axis( 1, at = xSim[prettyIndex], labels = xSimLab[prettyIndex],
                  las = labStyle )
        }
    }

    if (plotType == "b") {
        quantileBoxWidth <- quantileBoxWidth / 2
        for (i in 1 : simMatrix.nRows) {
            if ( !is.null(outerQuantile) && !is.na(outerQuantile) ) {
                lines( c(xSim[i], xSim[i]),
                       c(outerQuantile.low[i], outerQuantile.hig[i]),
                       col = simColor, lwd = 4)
            }
            box.lef <- xSim[i] - quantileBoxWidth
            box.rig <- xSim[i] + quantileBoxWidth
            if ( !is.null(innerQuantile) && !is.na(innerQuantile) ) {
                plot.addRange( c(box.lef, box.rig),
                               innerQuantile.hig[i], innerQuantile.low[i],
                               color = simColor )
            }
            if (plotMedian) {
                sim.median <- median(simMatrix[i, ], na.rm = T)
                lines( c(box.lef, box.rig), c(sim.median, sim.median),
                       col = medianColor, lwd = 2 )
            }
        }
        if (!is.null(realData)) {
            realDataLen <- length(realData)
            for (i in 1 : realDataLen) {
                points( xReal[i], realData[i], pch = 19, col = realColor, cex = realSize )
            }
        }

    } else {
        if ( !is.null(outerQuantile) && !is.na(outerQuantile) ) {
            ## Generate outer range's color
            outerCol <- col2rgb(simColor, alpha = F)
            outerCol <- rgb( outerCol[1], outerCol[2], outerCol[3],
                             alpha = 64, maxColorValue = 255 )
            plot.addRange( xSim, outerQuantile.hig, outerQuantile.low,
                        color = outerCol )
        }
        if ( !is.null(innerQuantile) && !is.na(innerQuantile) ) {
            plot.addRange( xSim, innerQuantile.hig, innerQuantile.low,
                           color = simColor )
        }
        if (plotMedian) {
            sim.median <- apply(simMatrix, 1, median, na.rm = T)
            lines( xSim, sim.median, col = medianColor, lwd = 2 )
        }
        if (!is.null(realData)) {
            lines( xReal, realData, type = "b", col = realColor, lwd = realSize )
        }
    }
}








########################################################################################################################

## Series are plotted in reverse order (the 1st series is on top, the last series is at the bottom).
## xaxp :  A vector of the form c(x1, x2, n) giving the coordinates of the extreme tick marks of the x-axis and the
##         number of intervals between tick-marks.
## yaxp :  Tick marks setting for the y-axis (see 'xaxp').
plot.timeSeries <- function( series, ranges.top = NULL, ranges.bot = NULL,
                             series.dates = NULL,
                             series.col = "black", ranges.col = "",
                             series.plotType = "l", series.lty = "solid", series.lwd = 2.5,
                             title = NULL,
                             xlim = NULL, ylim = NULL,
                             xaxp = NULL, yaxp = NULL,
                             xlab = "", ylab = "",
                             noXAxis = F, noYAxis = F, noBorder = F,
                             noPlot = F, noReturn = T,
                             vGrid.freqInMonth = 6, vGrid.col = "dark grey",
                             firstSeriesOnTop = T, useNumericDate = F) {


    toListType <- function( x, elementNum, fillNullForMissing = T ) {
        if ( is.list(x) ) {
            stopifnot( length(x) >= elementNum )

        } else {
            if (fillNullForMissing) {
                x <- list(x)
                if (elementNum > 1) {
                    for (i in 2 : elementNum) {
                        x[i] <- list(NULL)
                    }
                }

            } else {
                x <- rep( list(x), elementNum )
            }

        }

        return( x )
    }

    toVectorType <- function( x, elementNum ) {
        if ( length(x) > 1 ) {
            stopifnot( length(x) >= elementNum )
        } else {
            x <- rep( x, elementNum )
        }
        return( x )
    }


    nSeries <- 0
    if ( !is.null(series) ) {
        if (is.list(series)) {
            nSeries <- length(series)
        } else {
            nSeries <- 1
        }
    }
    nRanges <- 0
    if ( !is.null(ranges.top) ) {
        stopifnot( !is.null(ranges.bot) )
        if (is.list(ranges.top)) {
            stopifnot( is.list(ranges.bot) )
            stopifnot( length(ranges.bot) == length(ranges.top) )
            nRanges <- length(ranges.top)
        } else {
            nRanges <- 1
        }
    }
    nSeries <- max(nSeries, nRanges)
    if (nSeries <= 0) {
        return
    }

    series <- toListType(series, nSeries)
    ranges.top <- toListType(ranges.top, nSeries)
    ranges.bot <- toListType(ranges.bot, nSeries)

    series.dates <- toListType(series.dates, nSeries, fillNullForMissing = F)

    if (is.null(series.col)) series.col = ""
    if (is.null(ranges.col)) ranges.col = ""
    if (is.null(series.plotType)) series.plotType = "l"
    if (is.null(series.lty)) series.lty = "solid"

    series.col <- toVectorType(series.col, nSeries)
    ranges.col <- toVectorType(ranges.col, nSeries)
    series.plotType <- toVectorType(series.plotType, nSeries)
    series.lty <- toVectorType(series.lty, nSeries)

    series.lwd <- toVectorType(series.lwd, nSeries)

    xaxt <- "s"
    yaxt <- "s"
    bty <- "o"
    if (noXAxis) { xaxt <- "n" }
    if (noYAxis) { yaxt <- "n" }
    if (noBorder) { bty <- "n" }


    if ( is.null(xlim) ) {
        maxDate <- as.Date(-.Machine$integer.max, origin = "1970-01-01")
        minDate <- as.Date(.Machine$integer.max, origin = "1970-01-01")
    } else {
        minDate <- as.Date(xlim[1], origin = "1970-01-01")
        maxDate <- as.Date(xlim[2], origin = "1970-01-01")
    }

    for (i in 1 : length(series.dates)) {
        dates <- series.dates[[i]]
        if (is.null(dates)) {
            if ( !is.null(series[[i]]) && !is.null(names(series[[i]])) ) {
                dates <- names(series[[i]])
            } else if ( !is.null(ranges.top[[i]]) && !is.null(names(ranges.top[[i]])) ) {
                dates <- names(ranges.top[[i]])
            } else if ( !is.null(ranges.bot[[i]]) && !is.null(names(ranges.bot[[i]])) ) {
                dates <- names(ranges.bot[[i]])
            }
        }

        if (is.null(dates)) {
            stopifnot( length(series[[i]]) <= 1 && length(ranges.top[[i]]) <= 1 && length(ranges.bot[[i]]) <= 1 )

        } else {
            dates <- as.Date(dates, origin = "1970-01-01")
            if ( is.null(xlim) ) {
                if (min(dates) < minDate) { minDate <- min(dates) }
                if (max(dates) > maxDate) { maxDate <- max(dates) }
            } else {
                removedIndices <- which(dates < minDate | dates > maxDate)
                if (length(removedIndices) > 0) {
                    origNDates <- length(dates)
                    dates <- dates[-removedIndices]

                    if ( length(series[[i]])  == origNDates ) {
                        series[[i]] <- series[[i]][-removedIndices]
                    }
                    if ( length(ranges.top[[i]])  == origNDates ) {
                        ranges.top[[i]] <- ranges.top[[i]][-removedIndices]
                    }
                    if ( length(ranges.bot[[i]])  == origNDates ) {
                        ranges.bot[[i]] <- ranges.bot[[i]][-removedIndices]
                    }
                }
            }

            series.dates[[i]] <- dates
        }
    }

    ## The style of axis interval calculation to be used
    xaxs <- "r"    ## regular
    xlim <- c(minDate, maxDate)

    yaxs = "r"    ## regular
    if (!is.null(ylim)) {
        yaxs = "i"    ## internal
    } else {
        minY <- .Machine$double.xmax
        maxY <- .Machine$double.xmin
        for (i in 1 : nSeries) {
            if ( !is.null(series[[i]]) ) {
                if ( min(series[[i]], na.rm = T) < minY ) { minY <- min(series[[i]], na.rm = T) }
                if ( max(series[[i]], na.rm = T) > maxY ) { maxY <- max(series[[i]], na.rm = T) }
            }
            if ( !is.null(ranges.top[[i]]) && length(ranges.top[[i]]) > 1 )  {
                if ( min(ranges.top[[i]], na.rm = T) < minY ) { minY <- min(ranges.top[[i]], na.rm = T) }
                if ( max(ranges.top[[i]], na.rm = T) > maxY ) { maxY <- max(ranges.top[[i]], na.rm = T) }
            }
            if ( !is.null(ranges.bot[[i]]) && length(ranges.bot[[i]]) > 1 )  {
                if ( min(ranges.bot[[i]], na.rm = T) < minY ) { minY <- min(ranges.bot[[i]], na.rm = T) }
                if ( max(ranges.bot[[i]], na.rm = T) > maxY ) { maxY <- max(ranges.bot[[i]], na.rm = T) }
            }
        }
        ylim <- c(minY, maxY)
    }


    if (!noPlot) {
        ## Plot the main panel in blank
        if (useNumericDate) {
            allDates <- c(minDate : maxDate)
        } else {
            allDates <- as.Date( minDate : maxDate, origin = "1970-01-01" )
        }
        dummySeries <- rep(0, length(allDates))
        plot( allDates, dummySeries, type = "n", bty = bty,
              main = title,
              xaxt = xaxt, yaxt = yaxt, xaxs = xaxs, yaxs = yaxs,
              xaxp = xaxp, yaxp = yaxp,
              xlab = xlab, ylab = ylab,
              xlim = xlim, ylim = ylim,
              las = 1  ## Axes' labels are always horizontal
              )

        ## Vertical split every month
        if (vGrid.freqInMonth > 0) {
            gridMajorType <- "solid"
            gridMinorType <- "dashed"

            minYear <- as.numeric( substr(as.character(minDate), 1, 4) )
            maxYear <- as.numeric( substr(as.character(maxDate), 1, 4) )

            curYear <- minYear
            curMonth <- 1
            repeat {
                date <- as.Date( paste(curYear, curMonth, "01", sep = "-"), origin = "1970-01-01" )
                if (curMonth == 1) {
                    abline(v = date, col = vGrid.col, lty = gridMajorType)
                } else {
                    abline(v = date, col = vGrid.col, lty = gridMinorType)
                }

                if (curYear > maxYear) {
                    break
                }

                curMonth <- curMonth + vGrid.freqInMonth
                if (curMonth > 12) {
                    curYear <- curYear + floor(curMonth / 12)
                    curMonth <- curMonth %% 12
                }
            }
        }

        ## Plot ranges & series
        seriesIndices <- c(1 : nSeries)
        if (firstSeriesOnTop) {
            seriesIndices <- c(nSeries : 1)
        }
        for (i in seriesIndices) {
            center <- series[[i]]
            top <- ranges.top[[i]]
            bot <- ranges.bot[[i]]

            dates <- series.dates[[i]]
            if (is.null(dates)) {
                dates <- c(minDate, maxDate)
            }

            seriesCol <- series.col[i]
            rangeCol <- ranges.col[i]
            if ( seriesCol == "" && rangeCol != "" ) {
                ## Generate series' colors from ranges' colours
                seriesCol <- col2rgb(rangeCol, alpha = F)
                seriesCol <- rgb( seriesCol[1], seriesCol[2], seriesCol[3],
                                  alpha = 255, maxColorValue = 255 )

            } else if ( rangeCol == "" && seriesCol != "" ) {
                ## Generate ranges' colors from series' colours
                rangeCol <- col2rgb(seriesCol, alpha = F)
                rangeCol <- rgb( rangeCol[1], rangeCol[2], rangeCol[3],
                                 alpha = 64, maxColorValue = 255 )
            }

            if ( !is.null(top) && !is.null(bot) ) {
                plot.addRange(dates, top, bot, color = rangeCol)
            }

            if ( !is.null(center) ) {
                if ( length(center) == 1 && length(dates) != 1 ) {
                    lines( c(min(dates), max(dates)), c(center, center), col = seriesCol,
                           type = series.plotType[i], lty = series.lty[i], lwd = series.lwd[i] )
                } else {
                    lines( dates, center, col = seriesCol,
                           type = series.plotType[i], lty = series.lty[i], lwd = series.lwd[i] )
                }
            }
        }
    }

    if (!noReturn) {
        return( list(nSeries = nSeries, series = series,
                     ranges.top = ranges.top, ranges.bot = ranges.bot,
                     series.dates = series.dates,
                     xlim = xlim, ylim = ylim) )
    }
}



########################################################################################################################

plot.matrixRows <- function( dataMatrix, comparedData = NULL,
                             label = NULL, smoothDf = 30, ylim = NULL,
                             priCol = "dark orange", sndCol = "green", splineCol = "black",
                             priType = "l", sndType = "p",
                             nGridRows = 3, nvGrid.cols = 3 ) {




    par(mfrow=c(nGridRows, nvGrid.cols))

    rowNames <- rownames(dataMatrix)

    dataMatrix <- as.matrix(dataMatrix)

    for ( i in 1 : nrow(dataMatrix) ) {
        title <- rowNames[i]
        if ( !is.null(label) ) {
            title <- paste(label, "-", title)
        }

        spline <- smooth.splineWithNa(dataMatrix[i, ], df = smoothDf)

        plot.timeSeries( series = list(spline, dataMatrix[i, ], comparedData[i, ] ),
                         series.dates = NULL,
                         series.col = c(splineCol, priCol, sndCol),
                         series.plotType = c("l", priType, sndType),
                         series.lty = "solid", series.lwd = 2,
                         xlim = NULL, ylim = ylim,
                         title = title )
    }

    par(mfrow=c(1, 1))
    layout(1)
}



########################################################################################################################

## Series are plotted in reverse order (the 1st series is on top, the last series is at the bottom).
plot.multipleYearSeries <- function(
    series, series.dates = NULL,
    ranges.top = NULL, ranges.bot = NULL,
    firstSeries.quantile = NA, horizontalLine = NA,
    series.col = "black", ranges.col = "",
    series.plotType = "l", series.lty = "solid", series.lwd = 2.5,
    xlim = NULL, ylim = NULL, fixedAutoYLim = T,
    title = NULL, xlab = "", ylab = "",
    noXAxis = F, noYAxis = F, noBorder = T, panelGap = 0,
    vGrid.freqInMonth = 1, vGrid.col = "dark grey" ) {

    prePlot <- plot.timeSeries( series = series,
                                ranges.top = ranges.top, ranges.bot = ranges.bot,
                                series.dates = series.dates,
                                xlim = xlim, ylim = ylim,
                                noPlot = T, noReturn = F )

    minYear <- as.character(prePlot$xlim[1])
    minYear <- as.integer(substr(minYear, 1, 4))
    maxYear <- as.character(prePlot$xlim[2])
    maxYear <- as.integer(substr(maxYear, 1, 4))

    if (is.null(ylim) && fixedAutoYLim) {
        ylim <- prePlot$ylim
    }


    par( mar = c(0, 4, panelGap, 2) )
    nYears <- maxYear - minYear + 1

    titleHeight <- 0.1
    if (!is.null(title) && title != "") {
        titleHeight <- nYears * 0.05 + titleHeight
    }
    xAxisHeight <- 0.1
    if (!noXAxis) {
        xAxisHeight <- nYears * 0.03 + xAxisHeight
    }
    if ( !is.null(xlab) && xlab != "" ) {
        xAxisHeight <- nYears * 0.03 + xAxisHeight
    }
    layout( c(0, 1:nYears, 0), heights = c(titleHeight, rep(1, nYears), xAxisHeight) )

    curYear <- maxYear
    while (curYear >= minYear) {
        firstDayOfYear <- as.Date( paste(curYear, "01-01", sep = "-") )
        lastDayOfYear <- as.Date( paste(curYear, "12-31", sep = "-") )

        if (is.null(ylab) || ylab == "") {
            curYlab <- curYear
        } else {
            curYlab <- paste(ylab, " (", curYear, ")", sep = "")
        }

        if (curYear != minYear) {
            curNoXAxis = T
        } else {
            curNoXAxis = noXAxis
        }

        curPlot <- plot.timeSeries( series,
                                    ranges.top = ranges.top, ranges.bot = ranges.bot,
                                    series.dates = series.dates,
                                    series.col = series.col, ranges.col = ranges.col,
                                    series.plotType = series.plotType,
                                    series.lty = series.lty, series.lwd = series.lwd,
                                    xlim = c(firstDayOfYear, lastDayOfYear),
                                    ylim = ylim,
                                    title = NULL,
                                    xlab = "", ylab = curYlab,
                                    noXAxis = curNoXAxis, noYAxis = noYAxis, noBorder = noBorder,
                                    noPlot = F, noReturn = F,
                                    vGrid.freqInMonth = vGrid.freqInMonth, vGrid.col = vGrid.col )

        if ( !is.null(horizontalLine) && !is.na(horizontalLine) ) {
            abline( h = horizontalLine, col = "black", lwd = 1.5, lty = "dashed" )
        }

        if ( !is.null(firstSeries.quantile) &&
                 !is.na(firstSeries.quantile) &&
                 !is.null(curPlot$series[[1]]) ) {
            quantileLine <- quantile( curPlot$series[[1]], probs = firstSeries.quantile,
                                             na.rm = T, names = F )
            abline( h = quantileLine, col = "red", lwd = 1.5, lty = "dashed" )
        }

        if (curYear == maxYear && !is.null(title)) {
            mtext(title, side = 3, line = 1.5, outer = F, cex = 1.2)
        }

        curYear <- curYear - 1
    }

    if ( !is.null(xlab) && xlab != "" ) {
        if (noXAxis) {
            line <- 1.5
        } else {
            line <- 3.2
        }
        mtext(xlab, side = 1, line = line, outer = F, cex = 1)
    }

    layout(1)
    par( mar = c(5, 4, 4, 2) )
}



########################################################################################################################

hist.matrixRows <- function( dataMatrix, label = NULL, breaks = "Sturges",
                            nGridRows = 3, nvGrid.cols = 3 ) {

    dataMatrix <- as.matrix(dataMatrix)

    par(mfrow=c(nGridRows, nvGrid.cols))
    rowNames <- rownames(dataMatrix)

    for ( i in 1 : nrow(dataMatrix) ) {
        title <- rowNames[i]
        if ( !is.null(label) ) {
            title <- paste(label, "-", title)
        }

        hist( dataMatrix[i, ], main = title, breaks = breaks )
        #plot( ecdf(dataMatrix[i,]), main = title)
    }

    par(mfrow=c(1, 1))
    layout(1)
}



########################################################################################################################

acf.matrixRows <- function( dataMatrix, label = NULL, lag.max = NULL, verticalGrid = 365,
                            xlab = NULL ) {

    dataMatrix <- as.matrix(dataMatrix)
    rowNames <- rownames(dataMatrix)

    if (is.null(lag.max)) {
        lag.max <- ncol(dataMatrix) * 0.7
    }

    for ( i in 1 : nrow(dataMatrix) ) {
        title <- rowNames[i]
        if ( !is.null(label) ) {
            title <- paste(label, title, sep = "")
        }

        xaxt ='s'
        if ( !is.null(verticalGrid) && !is.na(verticalGrid) ) {
            xaxt ='n'
        }

        acf( dataMatrix[i, ], lag.max = lag.max, main = title, na.action = na.pass,
             col = "dark orange", ci.col  = "red",
             las = 1,  ## Axes' labels are always horizontal
             xaxt = xaxt, xlab = xlab,
             ylim = c(-0.5, 1.0) )

        if ( !is.null(verticalGrid) && !is.na(verticalGrid) ) {
            verLines <- seq(from = 0, to = lag.max, by = verticalGrid)
            abline(v = verLines, lty = "dashed", col = "black")
            axis(side = 1, at = verLines)
        }
    }
}



########################################################################################################################

plot.fft <- function(series, plotCutOff = NULL, xStep = 3) {
    if (is.null(plotCutOff)) {
        plotCutOff <- floor( length(series) / 2 )
    } else {
        plotCutOff <- plotCutOff + 1
    }

    series.fft <- fft(series)

    xx <- seq(0, plotCutOff - 1, by = xStep)
    xx.label <- xx
    xlab <- paste("Number of Cycles\n(within the time series of", length(series), "days)")
    # xx.label <- c( 0, round(length(series) / xx[2:length(xx)]) )
    # xlab <- "Period (days)"

    col <- ifelse( Mod(series.fft[1 : plotCutOff]) <= Mod(series.fft[1]), "dark grey", "black" )
    plot( c(0 : (plotCutOff - 1)),
          Mod(series.fft[1:plotCutOff]),
          type = "h", lwd = 7, col = col, xaxt ='n',
          xlab = xlab, ylab = "Power",
          las = 1 )

    abline(h = Mod(series.fft[1]), lty = "dashed")

    axis(1, at = xx, labels = xx.label, cex.axis = 0.8)

    return(series.fft)
}
