## ═════════════════════════════════════════════════════════════════════════════
## This file contains statistics-related utility functions.
## ═════════════════════════════════════════════════════════════════════════════


## —————————————————————————————————————————————————————————————————————————————
#' Compute the confidence intervals of the success rates of binomial samples
#' @description This function computes the confidence intervals of the success
#'              rates of binomial sampling.
#' @param nSuccess  A vector containing the numbers of successful tests.
#' @param nTested   A vector of the same size of the "nSuccess" vector,
#'                  containing the total numbers of tests.
#' @param confLevel The confidence interval level. Default: 0.95.
#' @return A data frame of 3 columns. The 1st column is the success rates of
#'         the samplings. The 2nd and the 3rd columns respectively contain the
#'         lower and the upper bounds of the confidence intervals of the success
#'         rates.
#' @export
computeBinomialConfInt <- function(nSuccess, nTested, confLevel = 0.95) {
    stopifnot( length(nTested) == length(nSuccess) )
    successRate <- nSuccess / nTested

    ciTop <- NULL
    ciBot <- NULL
    for (i in 1 : length(nTested)) {
        if (nTested[i] > 0) {
            ci <- binom.test(nSuccess[i], nTested[i],
                             conf.level = confLevel)$conf.int
            ciBot <- append(ciBot, ci[1])
            ciTop <- append(ciTop, ci[2])
        } else {
            ciBot <- append(ciBot, NA)
            ciTop <- append(ciTop, NA)
        }
    }

    return( data.frame(rate = successRate, ciBot = ciBot, ciTop = ciTop) )
}
