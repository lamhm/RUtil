## ═════════════════════════════════════════════════════════════════════════════
## This file contains functions to manipulate numeric data
## ═════════════════════════════════════════════════════════════════════════════


## —————————————————————————————————————————————————————————————————————————————
#' Convert a primitive variable or a vector of primitive variables into
#' number(s).
#' @param x A primitive variable or a vector of primitive variables that needs
#'          to be converted.
#' @return A number or a vector of number.
#' @export
toNumber <- function(x) {
    result <- as.numeric(as.character(x))
    return(result)
}


## —————————————————————————————————————————————————————————————————————————————
#' Factorise numbers into categories based on the given break points.
#' @description This function factorises numbers into categories based on the
#'              given break points.
#' @param x      A numeric vector containings all the numbers that need to be
#'               factorised.
#' @param breaks A numeric-typed or string-typed vector containing all break
#'               values. If a string-typed vector is received, these strings
#'               will be reused to name the factor levels of the output.
#' @param breakUnits  The unit of each break-points, defined by a vector (of the
#'                    same length of the number of break-points) of character
#'                    strings or a single character string.
#' @param intervalClosedSide This parameter can be set to be either "left",
#'                           "right", or "both" to indicates how numbers should
#'                           be grouped if their values equal to break values.
#'                           If the "left" value is chosen, numbers will be
#'                           categorised into left-closed intervals, i.e.
#'                           [b1 : b2), [b2 : b3), where b1, b2 and b3 are the
#'                           break values. If the "right" value is chosen, the
#'                           interval will be right-closed, i.e. (b1, b2] and
#'                           (b2, b3]. If the parameter is set to "both", both
#'                           sides of the intervals will be the same and
#'                           alternatedly swithched between being open and being
#'                           closed, i.e. [b1, b2], (b2, b3), [b3, b4].
#'                           Default: "left".
#' @param rangeNotation      A string that will be used to separate the two
#'                           boundaries of each interval. Default: " : ".
#' @param reverseLevelOrder  A logical value indicating whether the order of the
#'                           breaks should be reversed.
#' @param removeUnusedLevels A logical value indicating whether unused levels
#'                           should be removed from the result.
#' @return A factor-typed vector that contains the group of each of the given
#'         values in x.
#' @export
groupNumbers <- function( x,
                          breaks,
                          breakUnits         = "",
                          intervalClosedSide = c("left", "right", "both"),
                          rangeNotation      = " : ",
                          reverseLevelOrder  = FALSE,
                          removeUnusedLevels = FALSE ) {

    stopifnot( length(breaks) >= 1 )
    strMinusInfinity <- "-\u221E"  ## "-∞"
    strPlusInfinity  <- "+\u221E"  ## "+∞"

    ## Arrange the breaks
    numericBreaks <- toNumber(breaks)
    breakOrders   <- order(numericBreaks, decreasing = F)
    numericBreaks <- numericBreaks[breakOrders]
    nBreaks <- length(numericBreaks)

    if (length(breakUnits) == 1) {
        breakUnits <- rep(breakUnits, nBreaks)
    }
    if (length(breakUnits) != nBreaks) {
        stop("The number of breaks and the number of break units are mismatched.")
    }
    stringBreaks <- paste0(breaks, breakUnits)
    stringBreaks <- stringBreaks[breakOrders]

    ## Add -Inf as the left boundary and +Inf as the right boundary if needed
    x <- toNumber(x)
    minX <- min(x, na.rm = T)
    maxX <- max(x, na.rm = T)
    if ( minX < numericBreaks[1] ||
         (minX == numericBreaks[1] && intervalClosedSide == "right") ) {
        numericBreaks <- c( -Inf, numericBreaks )
        stringBreaks  <- c( strMinusInfinity, stringBreaks )
        nBreaks <- length(numericBreaks)
    }
    if ( maxX > numericBreaks[nBreaks] ||
         (maxX == numericBreaks[nBreaks] && intervalClosedSide != "right") ) {
        numericBreaks <- c( numericBreaks, +Inf )
        stringBreaks  <- c( stringBreaks, strPlusInfinity )
        nBreaks <- length(numericBreaks)
    }
    if ( numericBreaks[1] == -Inf ) {
        stringBreaks[1] <- strMinusInfinity
    }
    if ( numericBreaks[nBreaks] == +Inf ) {
        stringBreaks[nBreaks] <- strPlusInfinity
    }

    ## Categorise numbers
    intervalClosedSide <- intervalClosedSide[1]
    groupOfX <- rep( NA, length(x) )
    groupNames <- NULL

    closeBracketsAsDefault <- ifelse(numericBreaks[1] == -Inf, F, T)
    groupIdx <- 1
    while (groupIdx < nBreaks) {
        leftBracket  <- "("
        rightBracket <- ")"

        if ( intervalClosedSide == "right" ) {
            if ( numericBreaks[groupIdx + 1] != +Inf ) {
                rightBracket <- "]"
            }
            groupOfX[ which(numericBreaks[groupIdx] < x
                            & x <= numericBreaks[groupIdx + 1]) ] <- groupIdx

        } else if ( intervalClosedSide == "left" ) {
            if ( numericBreaks[groupIdx] != -Inf ) {
                leftBracket <- "["
            }
            groupOfX[ which(numericBreaks[groupIdx] <= x
                            & x < numericBreaks[groupIdx + 1]) ] <- groupIdx

        } else {
            ## intervalClosedSide == "both"
            if (closeBracketsAsDefault) {
                if ( numericBreaks[groupIdx] != -Inf ) {
                    leftBracket <- "["
                }
                if ( numericBreaks[groupIdx + 1] != +Inf ) {
                    rightBracket <- "]"
                }
                groupOfX[
                    which(numericBreaks[groupIdx] <= x
                          & x <= numericBreaks[groupIdx + 1]) ] <- groupIdx
            } else {
                groupOfX[
                    which(numericBreaks[groupIdx] < x
                          & x < numericBreaks[groupIdx + 1]) ] <- groupIdx
            }

        }

        groupNames[groupIdx] <-
            paste0( leftBracket, stringBreaks[groupIdx], rangeNotation,
                    stringBreaks[groupIdx + 1], rightBracket )
        groupIdx <- groupIdx + 1
        closeBracketsAsDefault <- !closeBracketsAsDefault
    }

    groupOfX[ which(x == -Inf) ] <- 1
    groupOfX[ which(x == +Inf) ] <- length(groupNames)
    groupOfX <- groupNames[groupOfX]

    if (removeUnusedLevels) {
        usedGroupNames <- NULL
        for (groupName in groupNames) {
            if ( sum(which(groupOfX == groupName)) > 0 ) {
                usedGroupNames <- append(usedGroupNames, groupName)
            }
        }
        groupNames <- usedGroupNames
    }

    if (reverseLevelOrder) {
        groupNames <- rev(groupNames)
    }

    groupOfX <- factor(groupOfX, levels = groupNames, ordered = T)
    return( groupOfX )
}



