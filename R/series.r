## ═════════════════════════════════════════════════════════════════════════════
## This file contains functions to process time series.
## ═════════════════════════════════════════════════════════════════════════════



splineWithNa <- function(x, y = NULL, df) {
    xIsDates <- T

    if (is.null(y)) {
        y <- x
        x <- names(y)
        if (is.null(x)) {
            x <- c(1 : length(y))
            xIsDates <- F
        }
    }
    if (is.character(x)) {
        x <- as.Date( x, origin = "1970-01-01" )
    }

    nonNaPos <- which(!is.na(y))
    nonNaPosNum <- length(nonNaPos)
    if ( nonNaPosNum > 1 ) {
        df = round( df * (nonNaPosNum / length(y)) )

        if (df > 1) {
            smoothLine <- smooth.spline(x[nonNaPos], y[nonNaPos], df = df)
            smoothLine.y <- smoothLine$y
            if (xIsDates) {
                names(smoothLine.y) <-
                    as.character( as.Date(smoothLine$x, origin = "1970-01-01") )
            } else {
                names(smoothLine.y) <- smoothLine$x
            }
            return(smoothLine.y)
        }
    }

    return(NULL)
}


## —————————————————————————————————————————————————————————————————————————————
#' Serise: Calculate the moving averages.
#' @param x The original series (if y is NULL), or the x-values of the series.
#'          The x-values can be automatically assigned based on the names of the
#'          elements of the given series).
#' @param y The y-values of the series. Default: NULL
#' @param windowSize The size of the moving window that will be used to
#'                   calculate the moving averages.
#' @param preserveMissing If this is set to be TRUE, moving averages will only
#'                        be calculated for the data points of which an original
#'                        value is available. Default: FALSE.
#' @return A new series that contains the moving averages of the given time
#'         series.
#' @export
seriesMovingAverage <- function( x, y = NULL,
                                 windowSize, preserveMissing = F ) {

    if ( is.null(y) ) {
        y <- x
        x <- NULL
    }

    seriesLength <- length(y)

    if ( is.null(x) ) {
        x <- names(y)
    }
    if ( is.null(x) ) {
        x <- 1 : seriesLength
    } else if ( isNumeric(x) ) {
        x <- as.numeric(x)
    } else if ( isDate(x, dateFormat = "%Y-%m-%d") ) {
        x <- as.Date(x, format = "%Y-%m-%d")
    }

    if ( length(x) != seriesLength ) {
        stop("x and y must be in the same length.")
    }
    if ( length(x) != length(unique(x)) ) {
        stop("X-values must be unique.")
    }

    ## Sort y by x
    y <- y[ order(x) ]
    x <- x[ order(x) ]

    result <- y
    halfWindowSize <- round (windowSize - 1) / 2

    for (idx in 1 : seriesLength) {
        currentX <- x[idx]
        windowFirstX <- currentX - halfWindowSize
        windowLastX <- currentX + halfWindowSize

        startIdx <- idx - halfWindowSize
        endIdx <- idx + halfWindowSize
        if (startIdx < 1) startIdx <- 1
        if (endIdx > seriesLength) endIdx <- seriesLength

        while (x[startIdx] < windowFirstX) startIdx <- startIdx + 1
        while (x[endIdx] > windowLastX) endIdx <- endIdx - 1

        result[idx] <- mean(y[startIdx : endIdx], na.rm = T)
    }

    if (preserveMissing) {
        result[ which(is.na(y)) ] <- NA    # preserve missing data points
    }

    names(result) <- x
    return(result)
}



## —————————————————————————————————————————————————————————————————————————————
#' Series: Interpolate missing data points (marked by NA) based on the available
#' data points.
#' @param x The original series (if y is NULL), or the x-values of the series.
#'          The x-values can be automatically assigned based on the names of the
#'          elements of the given series).
#' @param y The y-values of the series. Default: NULL
#' @param method The interpolating method. Only "linear" is supported for the
#'               current version. Default: "linear".
#' @return A new series with all the missing data points (NA values)
#'         interpolated.
#' @export
seriesInterpolate <- function( x, y = NULL, method = "linear" ) {
    if ( is.null(y) ) {
        y <- x
        x <- NULL
    }

    seriesLength <- length(y)

    if ( is.null(x) ) {
        x <- names(y)
    }
    if ( is.null(x) ) {
        x <- 1 : seriesLength
    } else if ( isNumeric(x) ) {
        x <- as.numeric(x)
    } else if ( isDate(x, dateFormat = "%Y-%m-%d") ) {
        x <- as.Date(x, format = "%Y-%m-%d")
    }

    if ( length(x) != seriesLength ) {
        stop("x and y must be in the same length.")
    }
    if ( length(x) != length(unique(x)) ) {
        stop("X-values must be unique.")
    }

    ## Sort y by x
    y <- y[ order(x) ]
    x <- x[ order(x) ]

    result <- y
    anchors <- which( !is.na(y) )

    slope <- NA
    interception <- NA
    for ( idx in 1 : seriesLength ) {
        if ( !is.na(result[idx]) ) {
            slope <- NA
            interception <- NA

        } else {
            if ( is.na(slope) ) {
                anchorDistances <- idx - anchors
                leftAnchors <- sort( anchors[which(anchorDistances > 0)],
                                     decreasing = T )
                rightAnchors <- sort( anchors[which(anchorDistances < 0)],
                                      decreasing = F )

                fstAnchor <- NA
                if ( length(leftAnchors) >= 1 ) {
                    fstAnchor <- leftAnchors[1]
                } else if ( length(rightAnchors) >= 2 ) {
                    fstAnchor <- rightAnchors[2]
                } else {
                    stop("At least 2 available data points are required.")
                }

                sndAnchor <- NA
                if ( length(rightAnchors) >= 1 ) {
                    sndAnchor <- rightAnchors[1]
                } else if ( length(leftAnchors) >= 2 ) {
                    sndAnchor <- leftAnchors[2]
                } else {
                    stop("At least 2 available data points are required.")
                }

                slope <- (y[fstAnchor] - y[sndAnchor]) /
                    (x[fstAnchor] - x[sndAnchor])
                interception <- y[fstAnchor] - slope * x[fstAnchor]
            }

            result[idx] <- slope * x[idx] + interception
        }
    }

    names(result) <- x
    return(result)
}

