## ═════════════════════════════════════════════════════════════════════════════
## This file contains functions for string processing.
## ═════════════════════════════════════════════════════════════════════════════


## —————————————————————————————————————————————————————————————————————————————
#' Concatenate strings
#' @description This function concatenates two strings together without adding
#'              anything in between. It is a wrapper of the paste0() function
#'              of the base package.
#' @param firstString  The first string.
#' @param secondString The second string.
#' @return A concatenation of the two given strings.
#' @usage  firstString s+ secondString
#' @rdname stringConcatenate
#' @export
`%_%` <- function(firstString, secondString) {
    return( paste0(firstString, secondString) )
}



## —————————————————————————————————————————————————————————————————————————————
#' Print decoration strings
#' @description This function print out a few strings that may be used to
#'              decorate R codes in order to make the codes easier to read.
#' @export
printDecorStrings <- function() {
    cat("\n")
    cat("╔═╗ ╚═╝ ║   ┌─┐ └─┘ │\n")
    cat("## ═════════════════════════════════════════════════════════" %+%
            "════════════════════\n")
    cat("## —————————————————————————————————————————————————————————" %+%
            "————————————————————\n")
    cat("\n")
}



## —————————————————————————————————————————————————————————————————————————————
#' Search for a string that matches a given pattern.
#' @description This function looks into a string pool and searches for a string
#'              that matches the pattern <prefix>_<coreString>_<suffix>.
#' @param stringPool A string pool, from which the required string will be
#'                   looked for.
#' @param coreString The core of the required string. This is the
#'                   always-required part of the string in need.
#' @param prefixOptions A vector containing all the options for the prefix of
#'                      the required string. Note that these options will only
#'                      be considered after the empty-prefix has been tested.
#'                      Default: NULL (no prefixes).
#' @param suffixOptions A vector containing all the options for the suffix of
#'                      the required string. Note that these options will only
#'                      be considered after the empty-suffix has been tested.
#'                      Default: NULL (no suffixes).
#' @param sep A string that will be used to connect the core string with
#'            non-empty prefixes and non-empty suffixes.
#' @return The first string in the given string pool that matches all the given
#'         criteria. If such string is not found, this function will return a
#'         NULL value.
#' @note   This function prioritises empty prefix/suffix over non-empty
#'         prefixes/suffixes, and prioritises prefixes over suffixes.
#' @export
searchString <- function( stringPool, coreString,
                          prefixOptions = NULL,
                          suffixOptions = NULL,
                          sep = "_" ) {

    searchList <- coreString

    if ( !is.null(prefixOptions) ) {
        newSearchList <- NULL
        for (prefix in prefixOptions) {
            for (stem in searchList) {
                newSearchList <-
                    append( newSearchList, paste(prefix, stem, sep = sep) )
            }
        }
        searchList <- append(searchList, newSearchList)
    }

    if ( !is.null(suffixOptions) ) {
        newSearchList <- NULL
        for (suffix in suffixOptions) {
            for (stem in searchList) {
                newSearchList <-
                    append( newSearchList, paste(stem, suffix, sep = sep) )
            }
        }
        searchList <- append( searchList, newSearchList )
    }

    searchList <- unique(searchList, fromLast = F)

    for (name in searchList) {
        if (name %in% stringPool) {
            return(name)
        }
    }
    return(NULL)
}

