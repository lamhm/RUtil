## ═════════════════════════════════════════════════════════════════════════════
## This file contains functions to handle colors that used for plotting.
## ═════════════════════════════════════════════════════════════════════════════


## —————————————————————————————————————————————————————————————————————————————
#' Invert colours
#' @description This function generates the inverse colours of a given list of
#'              colour.
#' @param colours A vector of colours of which the inverses need to be
#'                generated.
#' @return A vector containing the inverse colours.
#' @export
colourInvert <- function(colours) {
    newColours <- NULL
    for (colour in colours) {
        rgbValue <- col2rgb(colour, alpha = T)
        newColour <- rgb( 255 - rgbValue[1], 255 - rgbValue[2],
                          255 - rgbValue[3], alpha = rgbValue[4],
                          maxColorValue = 255 )
        newColours <- append(newColours, newColour)
    }

    return(newColours)
}


## —————————————————————————————————————————————————————————————————————————————
#' Set colours transparency
#' @description   This function changes the alpha channel of given colours.
#' @param colours A colour or a vector of colours, of which the RGB channels
#'                will be extracted.
#' @param alphas  A value or a vector of values for the alpha channel. If a
#'                vector is passed, this vector must have the same length with
#'                the colours vector. If only a single value is given, this
#'                value will be used for the alpha channel of all the given
#'                colours. When an alpha value is less than or equal to 1, the
#'                value is interpreted as proportion (ranging in [0, 1]). When
#'                an alpha value is greater than 1, it is processed as a colour
#'                code (ranging in [0, 255]). Default value: 1 (fully opaque).
#' @return A vector of new colours with the R, G, B channels taken from the
#'         original colours, and the alpha channel taken from the given alpha.
#' @export
colourSetTransparency <- function(colours, alphas = 1) {
    nColours <- length(colours)
    nAlphas <- length(alphas)
    stopifnot( nAlphas == 1 || nAlphas == nColours )

    newColours <- NULL
    for (colourIdx in 1 : nColours) {
        colour <- colours[colourIdx]
        alpha <- ifelse( nAlphas == 1, alphas[1], alphas[colourIdx] )

        if (alpha < 0) {
            stop( "Invalid value for the alpha channel " %+%
                     "(a non-negative number is expected)." )

        } else if (alpha > 255) {
            stop( "Invalid value for the alpha channel " %+%
                      "(only support values between 0 and 255)." )

        } else if (alpha <= 1) {
            alpha <- round(alpha * 255)
        }

        rgbValue <- col2rgb(colour, alpha = F)
        newColour <- rgb( rgbValue[1], rgbValue[2], rgbValue[3],
                          alpha = alpha, maxColorValue = 255 )
        newColours <- append(newColours, newColour)
    }

    names(newColours) <- names(colours)
    return(newColours)
}


## —————————————————————————————————————————————————————————————————————————————
#' Generate a colour-blind-proof colour palette.
#' @description This function generates a colour palette with a given number
#'              of colours. This palette is colour-blind proof.
#' @param n     The number of colours that will be generated for the palette.
#'              This colour palette only supports having between 1 and 5 colours
#'              (n in [1:5]). Default value: 5.
#' @param alpha The transparency of the colours in the palette.
#'              Default value: 1 (fully opaque).
#' @param forDarkBg If this logical argument is TRUE, the function will return
#'                  brighter colours that are suitable to be used on a dark
#'                  background. Default value: FALSE.
#' @return A vector containing n colours.
#' @export
colourPaletteForCBlind <- function(n = 5, alpha = 1, forDarkBg = FALSE) {
    colours <- c( dark_blue = "#0b486b",
                  teal      = "#3cafa0",
                  grey      = "#777777",
                  orange    = "#fd9644",
                  dark_red  = "#be2b4e" )

    if (forDarkBg) {
        colours <- c( purple     = "#9c27b0",
                      cyan       = "#00bcd4",
                      light_grey = "#cccccc",
                      lime       = "#eeff41",
                      orange     = "#ff5722" )
    }

    maxN <- length(colours)
    if (n > maxN) {
        stop("The palette does not support more than " %+% maxN %+% " colours.")
    } else if (n == 4) {
        colours <- colours[c(1, 2, 4, 5)]
    } else if (n == 3) {
        colours <- colours[c(1, 3, 5)]
    } else if (n == 2) {
        colours <- colours[c(1, 5)]
    } else if (n == 1) {
        colours <- colours[1]
    } else if (n <= 0) {
        stop("The number of colours must be positive.")
    }

    colours <- colourSetTransparency(colours = colours, alphas = alpha)
    return(colours)
}

