## ═════════════════════════════════════════════════════════════════════════════
## This file contains functions for plotting data.
## ═════════════════════════════════════════════════════════════════════════════



## —————————————————————————————————————————————————————————————————————————————
#' Plot a blank graph panel
#' @description This function plot a blank graph panel of which the x-axis and
#'              the y-axis have predefined scales.
#' @param x        X-values of the data points that will be used to calculate
#'                 the range of the x-axis. X-values can be either factors or
#'                 numbers.
#' @param y        Y-values of the data points that will be used to calculate
#'                 the range of the y-axis. Y-values must be of numeric type.
#' @param xlim     The range of the x-axis.
#'                 Default: NULL (to be calculated from the x argument).
#' @param ylim     The range of the y-axis
#'                 Default: NULL (to be calculated from the y argument).
#' @param bgColour The back ground colour to be filled in the plot area.
#'                 Default: NULL (no fill).
#' @param ... Additional graphical parameters that will be passed to the
#'            built-in plot method.
#' @note  If xlim is specified, the x argument will be ignored. Similarly, if
#'        ylim is specified, the y argument will be ignored.
#' @note  Even with both the x and the y arguments specified, the corresponding
#'        data points will only be used for the calculation of xlim and ylim
#'        and will NOT be plotted.
#' @export
plotBlankGraph <- function( x = NULL, y = NULL,
                            xlim = NULL, ylim = NULL,
                            xlab = NULL, ylab = NULL,
                            xaxt = "s",
                            bgColour = NULL, ... ) {

    if ( is.null(x) && is.null(xlim) ) {
        stop("Either x or xlim must be specified.")
    }
    if ( is.null(y) && is.null(ylim) ) {
        stop("Either y or ylim must be specified.")
    }

    if ( is.null(xlim) ) {
        x <- unlist(x)
        if ( !is.numeric(x) && !is.factor(x) ) {
            stop("Values of x must be either factors or numbers.")
        }
        xNumeric <- as.numeric(x)
        xlim <- c( min(xNumeric), max(xNumeric) )
    }

    if ( is.null(ylim) ) {
        y <- unlist(y)
        if ( !is.numeric(y) ) {
            stop("Values of y must be of the numeric type.")
        }
        ylim <- c( min(y), max(y) )
    }

    if ( is.null(xlab) ) {
        xlab <- ""
    }
    if ( is.null(ylab) ) {
        ylab <- ""
    }

    if ( is.factor(x) && xaxt == "s" ) {
        plot( NULL, xlim = xlim, ylim = ylim, xlab = xlab, ylab = ylab,
              xaxt = "n", ... )
        axisLabels <- sort( unique(x) )
        axis( 1, at = axisLabels, labels = axisLabels )

    } else {
        plot( NULL, xlim = xlim, ylim = ylim, xlab = xlab, ylab = ylab,
              xaxt = xaxt, ... )
    }

    if ( !is.null(bgColour) ) {
        plotArea <- par("usr")
        rect( plotArea[1], plotArea[3], plotArea[2], plotArea[4],col = bgColour )
    }
}



## —————————————————————————————————————————————————————————————————————————————
#' Plot data ranges
#' @description This function plots data ranges and the centers of the ranges
#'              (if given). The ranges can be plotted as boxes, whiskers, or
#'              a closed area. The center points can be plotted as lines,
#'              points, or both (i.e. connected points).
#' @param x       A vector containing x-values of the data.
#' @param yLow    A vector containing low-end y-values of the data.
#' @param yHigh   A vector containing high-end y-values of the data.
#' @param yCenter A vector containing center y-values of the data.
#'                Default: NULL (no center points)
#' @param newPlot A logical parameter indicating whether a new plot panel will
#'                be created for this plotting function. If this parameter is
#'                set to be FALSE, the ranges will be added onto an existing
#'                plot. Default: TRUE.
#' @param type       A character-typed parameter that specifies the plotting
#'                   type for the ranges. Available options are "box",
#'                   "whisher", and "area". Default: "box".
#' @param centerType A character-typed parameter that specifies the plotting
#'                   type for the center points. Available options are "line",
#'                   "point", and "both". Default: NA (the center type will
#'                   be automatically determined based on the range type).
#' @param centerColour A colour or a vector of colours to be used to plot the
#'                     centers. If the number of center points is greater than
#'                     the number of colours, the colours will be circulated.
#'                     Default: "black".
#' @param rangeColour  A colour or a vector of colours to be used to plot the
#'                     ranges. If the number of ranges is greater than the
#'                     number of colours, the colours will be circulated.
#'                     Default: "grey".
#' @param border       A colour to be used to plot the borders of the ranges.
#'                     Default: NA (borders will not be plotted).
#' @param boxWidth     A numeric value indicating the width of the boxes if
#'                     "box" is selected for the plotting type of the ranges.
#'                     Default: NA (the box width will be calculated
#'                     automatically).
#' @param centerGraphicParams A list of additional graphical parameters to be
#'                            used when plotting the center points.
#'                            Default: NULL.
#' @param rangeGraphicParams  A list of additional graphical parameters to be
#'                            used when plotting the ranges. Default: NULL.
#' @param ... Additional graphical parameters that will be used when creating
#'            a new plot panel. Note that these parameters will only be
#'            considered when newPlot is set to be TRUE.
#' @export
plotRanges <- function( x, yLow, yHigh, yCenter = NULL,
                        newPlot = TRUE,
                        type = c("box", "whisker", "area"),
                        centerType = c(NA, "line", "point", "both"),
                        centerColour = "black",
                        rangeColour = "grey",
                        border = NA,
                        boxWidth = NULL,
                        centerGraphicParams = NULL,
                        rangeGraphicParams = NULL,
                        ... ) {

    ## Inner utility function
    getYEndAt <- function(index, yLow, yHigh, end = c("low", "high")) {
        if (length(yLow) == 1) {
            yLowVal <- yLow[1]
        } else {
            yLowVal <- yLow[index]
        }
        if (length(yHigh) == 1) {
            yHighVal <- yHigh[1]
        } else {
            yHighVal <- yHigh[index]
        }
        end <- end[1]
        if ( end == "high" ) {
            return( max(yHighVal, yLowVal) )
        } else if ( end == "low" ) {
            return( min(yHighVal, yLowVal) )
        } else {
            stop("Invalid value for the 'end' argument.")
        }
    }


    ## Data validation
    x     <- unlist(x)
    yHigh <- unlist(yHigh)
    yLow  <- unlist(yLow)

    if ( !is.numeric(x) && !is.factor(x) ) {
        stop("The values of x must either be factors or numbers.")
    }
    if ( !is.numeric(yLow) || !is.numeric(yHigh) ) {
        stop("The values of yLow and yHigh must be of numeric type.")
    }
    if ( length(yHigh) > 1 && length(yLow) > 1
         && length(yHigh) != length(yLow) ) {
        stop("yLow and yHigh must be in the same length.")
    }

    yLength <- max( length(yLow), length(yHigh) )
    if ( length(x) != yLength ) {
        stop("x and y's must be in the same length.")
    }

    if ( !is.null(yCenter) ) {
        yCenter <- unlist(yCenter)
        if ( length(yCenter) != yLength ) {
            stop("x and y's must be in the same length.")
        }
        if ( !is.numeric(yCenter) ) {
            stop("The values of yCenter must be of numeric type.")
        }
    }

    realYHigh <- NULL
    realYLow  <- NULL
    for (idx in 1 : yLength) {
        realYHigh <- append( realYHigh, getYEndAt(idx, yLow = yLow,
                                                  yHigh = yHigh, end = "high") )
        realYLow  <- append( realYLow, getYEndAt(idx, yLow = yLow,
                                                 yHigh = yHigh, end = "low") )
    }

    if ( is.null(boxWidth) ) {
        xUnique <- sort( unique(as.numeric(x)) )
        xDiff <- xUnique[-1] - xUnique[-length(xUnique)]
        minXDiff <- min(xDiff)
        boxWidth <- minXDiff / 2
    }


    ## Real plotting starts from here
    if (newPlot) {
        plotBlankGraph( x = x, y = list(yLow, yHigh, yCenter), ... )
    }
    if ( is.factor(x) ) {
        x <- as.numeric(x)
    }

    type <- tolower( type[1] )
    centerType <- tolower( centerType[1] )

    ## Plot the ranges
    defaultGraphicParams <- par(rangeGraphicParams)

    if ( type == "box" || type == "b" ) {
        halfWidth <- boxWidth / 2
        nColours <- length(rangeColour)
        for (idx in 1 : yLength) {
            colourIdx <- ifelse( idx %% nColours, idx %% nColours, nColours )
            polyX <- c( x[idx] - halfWidth, x[idx] + halfWidth,
                        x[idx] + halfWidth, x[idx] - halfWidth )
            polyY <- c( realYHigh[idx], realYHigh[idx],
                        realYLow[idx] , realYLow[idx] )
            polygon( polyX, polyY, col = rangeColour[colourIdx],
                     border = border )
        }

    } else if ( type == "whisker" || type == "w" ) {
        nColours <- length(rangeColour)
        for (idx in 1 : yLength) {
            colourIdx <- ifelse( idx %% nColours, idx %% nColours, nColours )
            lines( x = c(x[idx], x[idx]), y = c(realYHigh[idx], realYLow[idx]),
                   col = rangeColour[colourIdx] )
        }

    } else if ( type == "area" || type == "a" ) {
        polyX <- append( x, rev(x) )
        polyY <- append( realYHigh, rev(realYLow) )
        polygon( polyX, polyY, col = rangeColour[1], border = border )

    } else {
        par(defaultGraphicParams)
        stop("The specified plot type (" %+% type %+% ") is not supported.")
    }

    par(defaultGraphicParams)


    ## Determine the plot type for the center points
    type <- substring(type, 1, 1)
    if ( is.na(centerType) ) {
        if ( type == "w" ) {
            centerType <- "point"
        } else {
            centerType <- "line"
        }
    }
    centerType <- substring(centerType, 1, 1)


    ## Plot the centers
    if ( !is.null(yCenter) ) {
        defaultGraphicParams <- par(centerGraphicParams)

        if ( type == "b" && centerType == "l" ) {
            halfWidth <- boxWidth / 2
            nColours  <- length(centerColour)
            for (idx in 1 : yLength) {
                xVal <- x[idx]
                yVal <- yCenter[idx]
                if ( !is.na(xVal) && !is.na(yVal) ) {
                    colourIdx <- ifelse( idx %% nColours, idx %% nColours,
                                         nColours )
                    lines( x = c(xVal - halfWidth, xVal + halfWidth),
                           y = c(yVal, yVal), col = centerColour[colourIdx] )
                }
            }

        } else {
            points( x = x, y = yCenter, type = centerType, col = centerColour )
        }

        par(defaultGraphicParams)
    }
}


