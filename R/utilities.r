## ═════════════════════════════════════════════════════════════════════════════
## This file contains utility functions that were created for convenience only.
## ═════════════════════════════════════════════════════════════════════════════


## —————————————————————————————————————————————————————————————————————————————
#' Check whether a given object is convertible to the numeric type
#' @param obj A data object that needs to be tested.
#' @return TRUE if the given object can be converted to the numeric type; FALSE
#'         otherwise.
#' @export
isNumeric <- function( obj ) {
    return( suppressWarnings(all(!is.na(as.numeric(as.character(obj))))) )
}



## —————————————————————————————————————————————————————————————————————————————
#' Check whether a given object is convertible to the Date type.
#' @param obj A data object that needs to be tested.
#' @param dateFormat The date format to which the object will be compared.
#'                   Default: "\%Y-\%m-\%d".
#' @return TRUE if the given object can be converted to the Date type; FALSE
#'         otherwise.
#' @export
isDate <- function( obj, dateFormat = "%Y-%m-%d" ) {
    return( suppressWarnings(all(!is.na(as.Date(as.character(obj),
                                                format = dateFormat)))) )
}

