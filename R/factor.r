## ═════════════════════════════════════════════════════════════════════════════
## This file contains functions to manipulate factors
## ═════════════════════════════════════════════════════════════════════════════


## —————————————————————————————————————————————————————————————————————————————
#' Reverse the level order of a list of factors.
#' @param x A vector of factors.
#' @return A vector of the same size of the input vector. The factors in this
#'         vector will have the same character presentation as the input vector;
#'         however, the order of the levels of these factors are reversed.
#' @export
revLevelOrder <- function(x) {
    if( !is.factor(x) ) {
        stop("The input value(s) is/are not factor(s).")
    }

    levelList <- rev( levels(x) )
    result <- factor(x, levels = levelList, ordered = T)

    return(result)
}
