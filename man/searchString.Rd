% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/string.r
\name{searchString}
\alias{searchString}
\title{Search for a string that matches a given pattern.}
\usage{
searchString(stringPool, coreString, prefixOptions = NULL,
  suffixOptions = NULL, sep = "_")
}
\arguments{
\item{stringPool}{A string pool, from which the required string will be
looked for.}

\item{coreString}{The core of the required string. This is the
always-required part of the string in need.}

\item{prefixOptions}{A vector containing all the options for the prefix of
the required string. Note that these options will only
be considered after the empty-prefix has been tested.
Default: NULL (no prefixes).}

\item{suffixOptions}{A vector containing all the options for the suffix of
the required string. Note that these options will only
be considered after the empty-suffix has been tested.
Default: NULL (no suffixes).}

\item{sep}{A string that will be used to connect the core string with
non-empty prefixes and non-empty suffixes.}
}
\value{
The first string in the given string pool that matches all the given
        criteria. If such string is not found, this function will return a
        NULL value.
}
\description{
This function looks into a string pool and searches for a string
             that matches the pattern <prefix>_<coreString>_<suffix>.
}
\note{
This function prioritises empty prefix/suffix over non-empty
        prefixes/suffixes, and prioritises prefixes over suffixes.
}
