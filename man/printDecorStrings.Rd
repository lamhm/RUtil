% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/string.r
\name{printDecorStrings}
\alias{printDecorStrings}
\title{Print decoration strings}
\usage{
printDecorStrings()
}
\description{
This function print out a few strings that may be used to
             decorate R codes in order to make the codes easier to read.
}
